require "bundler/inline"

gemfile(true, quiet: true) do
  source "https://rubygems.org"

  if ENV.key?('RAILS7')
    gem 'rails', '~> 7.0', '>= 7.0.4'
  else
    gem 'rails', '~> 6.1', '>= 6.1.7'
  end

  gem 'sqlite3'
  gem 'pry-rails'
  gem 'pry-nav', '~> 1.0'
end

require "active_record"
require "logger"
require "minitest/autorun"

null_logger = Logger.new('/dev/null')
ActiveRecord::Base.establish_connection(adapter: "sqlite3", database: ":memory:")
ActiveRecord::Base.logger = null_logger

ActiveRecord::Schema.define do
  create_table :projects, force: true do |t|
  end

  create_table :pipelines, force: true do |t|
    t.integer :project_id
    t.integer :partition_id
  end

  create_table :builds, force: true do |t|
    t.integer :pipeline_id
    t.integer :partition_id
    t.string :name
  end

  create_table :builds_metadata, force: true do |t|
    t.integer :build_id
    t.integer :partition_id
    t.boolean :test_flag, default: false
  end
end

class PartitionedRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.partitionable_model?
    true
  end

  if ENV.key?('RAILS7')
    def _primary_key_constraints_hash
      super.merge(partition_id: partition_id)
    end

    module AbstractReflectionPatch
      def join_scope(table, foreign_table, foreign_klass)
        klass_scope = super

        foreign_partition_key = options[:foreign_partition_key]
        if foreign_partition_key
          klass_scope.where!(table[:partition_id].eq(foreign_table[foreign_partition_key]))
        end

        klass_scope
      end
    end

    module MacroReflectionPatch
      def scope_for(relation, owner = nil)
        if scope.arity == 1 && owner.nil?
          relation
        else
          super
        end
      end
    end

    module AssociationReflectionPatch
      def check_eager_loadable!
        if scope && scope.arity == 1 && klass.try(:partitionable_model?)
          return
        else
          super
        end
      end
    end

    module AssociationOptionsPatch
      extend ActiveSupport::Concern

      class_methods do
        def valid_options(options)
          super + [:foreign_partition_key]
        end
      end
    end

    ActiveRecord::Reflection::AbstractReflection.prepend(AbstractReflectionPatch)
    ActiveRecord::Reflection::MacroReflection.prepend(MacroReflectionPatch)
    ActiveRecord::Reflection::AssociationReflection.prepend(AssociationReflectionPatch)
    ActiveRecord::Associations::Builder::Association.prepend(AssociationOptionsPatch)
  else
    require_relative './preloader.rb'

    def _update_row(attribute_names, attempted_action = "update")
      self.class._update_record(
        attributes_with_values(attribute_names),
        @primary_key => id_in_database,
        partition_id: partition_id
      )
    end

    def _delete_row
      self.class._delete_record(@primary_key => id_in_database, partition_id: partition_id)
    end

    module AssociationOptionsPatch
      extend ActiveSupport::Concern

      class_methods do
        def valid_options(options)
          super + [:foreign_partition_key]
        end
      end
    end

    module AbstractReflectionPatch
      def join_scope(table, foreign_table, foreign_klass)
        klass_scope = super

        foreign_partition_key = options[:foreign_partition_key]
        if foreign_partition_key
          klass_scope.where!(table[:partition_id].eq(foreign_table[foreign_partition_key]))
        end

        klass_scope
      end
    end

    module AssociationReflectionPatch
      def join_scopes(table, predicate_builder, klass = self.klass, record = nil) # :nodoc:
        if scope
          [scope_for(build_scope(table, predicate_builder, klass), record)]
        else
          []
        end
      end

      def check_preloadable!
        if scope && scope.arity == 1 && klass.try(:partitionable_model?)
          return
        else
          super
        end
      end
      alias :check_eager_loadable! :check_preloadable!
    end

    module MacroReflectionPatch
      def scope_for(relation, owner = nil)
        if scope.arity == 1 && owner.nil?
          relation
        else
          relation.instance_exec(owner, &scope) || relation
        end
      end
    end

    module ThroughReflectionPatch
      def join_scopes(table, predicate_builder, klass = self.klass, record = nil) # :nodoc:
        source_reflection.join_scopes(table, predicate_builder, klass, record) + super
      end
    end

    module PolymorphicReflectionPatch
      def join_scopes(table, predicate_builder, klass = self.klass, record = nil) # :nodoc:
        scopes = @previous_reflection.join_scopes(table, predicate_builder, record) + super
        scopes << build_scope(table, predicate_builder, klass).instance_exec(record, &source_type_scope)
      end
    end

    module WhereClausePatch
      alias :eql? :==

      def hash
        [self.class, predicates].hash
      end
    end

    module RelationPatch
      def values_for_queries # :nodoc:
        @values.except(:extending, :skip_query_cache, :strict_loading)
      end

      def preload_associations(records) # :nodoc:
        preload = preload_values
        preload += includes_values unless eager_loading?
        scope = strict_loading_value ? StrictLoadingScope : nil
        preload.each do |associations|
          Gitlab::ActiveRecord::Associations::Preloader.new(records: records, associations: associations, scope: scope).call
        end
      end
    end

    ActiveRecord::Associations::Builder::Association.prepend(AssociationOptionsPatch)
    ActiveRecord::Reflection::AssociationReflection.prepend(AssociationReflectionPatch)
    ActiveRecord::Reflection::MacroReflection.prepend(MacroReflectionPatch)
    ActiveRecord::Reflection::ThroughReflection.prepend(ThroughReflectionPatch)
    ActiveRecord::Reflection::PolymorphicReflection.prepend(PolymorphicReflectionPatch)
    ActiveRecord::Relation.prepend(RelationPatch)
    ActiveRecord::Relation::WhereClause.prepend(WhereClausePatch)
    ActiveRecord::Reflection::AbstractReflection.prepend(AbstractReflectionPatch)
  end
end

class Project < ActiveRecord::Base
  has_many :pipelines
end

class Pipeline < PartitionedRecord
  belongs_to :project

  has_many :builds,
    -> (pipeline) { where(partition_id: pipeline.partition_id) },
    foreign_partition_key: :partition_id,
    dependent: :destroy # else delete_all nullifies the relation
end

class Build < PartitionedRecord
  belongs_to :pipeline,
    -> (build) { where(partition_id: build.partition_id) },
    foreign_partition_key: :partition_id

  has_one :metadata,
    -> (build) { where(partition_id: build.partition_id) },
    foreign_key: :build_id,
    foreign_partition_key: :partition_id,
    inverse_of: :build,
    autosave: true

  accepts_nested_attributes_for :metadata
end

class Metadata < PartitionedRecord
  self.table_name = :builds_metadata

  belongs_to :build,
    -> (metadata) { where(partition_id: metadata.partition_id) }
end

## Test helpers

class QueryRecorder
  attr_reader :log

  def initialize(&block)
    @log = []

    ActiveRecord::Base.connection.unprepared_statement do
      ActiveSupport::Notifications.subscribed(method(:callback), 'sql.active_record', &block)
    end
  end

  def callback(name, start, finish, message_id, values)
    @log << values[:sql]
  end

  def self.log
    result = new do
      ActiveRecord::Base.transaction do
        yield
        raise ActiveRecord::Rollback
      end
    end

    result.log
  end
end

## Data setup

project = Project.create!

[100, 200, 300].each do |partition_id|
  5.times do
    pipeline = Pipeline.create!(project: project, partition_id: partition_id)

    3.times do
      job = Build.create!(pipeline: pipeline, partition_id: partition_id)
      metadata = Metadata.create!(build: job, partition_id: partition_id)
    end
  end
end

# create
# find
# save
# update
# delete
# destroy
# delete_all
# destroy_all
# count

describe 'Single model queries' do
  it 'creates using id and partition_id' do
    create_statement = "INSERT INTO \"builds\" (\"pipeline_id\", \"partition_id\") VALUES (1, 100)"
    pipeline = Pipeline.first

    result = QueryRecorder.log do
      Build.create!(pipeline_id: pipeline.id, partition_id: pipeline.partition_id)
    end

    _(result).must_include(create_statement)
  end

  it 'finds with id and partition_id' do
    find_statement = "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"id\" = 1 AND \"builds\".\"partition_id\" = 100 LIMIT 1"

    result = QueryRecorder.log do
      Build.find_by!(id: 1, partition_id: 100)
    end

    _(result).must_include(find_statement)
  end

  it 'saves using id and partition_id' do
    update_statement = "UPDATE \"builds\" SET \"name\" = 'test' WHERE \"builds\".\"id\" = 1 AND \"builds\".\"partition_id\" = 100"
    build = Build.find_by!(id: 1, partition_id: 100)

    result = QueryRecorder.log do
      build.name = 'test'

      build.save!
    end

    _(result).must_include(update_statement)
  end

  it 'updates using id and partition_id' do
    update_statement = "UPDATE \"builds\" SET \"name\" = 'test2' WHERE \"builds\".\"id\" = 1 AND \"builds\".\"partition_id\" = 100"
    build = Build.find_by!(id: 1, partition_id: 100)

    result = QueryRecorder.log do
      build.update!(name: 'test2')
    end

    _(result).must_include(update_statement)
  end

  it 'deletes using id and partition_id' do
    delete_statement = "DELETE FROM \"builds\" WHERE \"builds\".\"id\" = 2 AND \"builds\".\"partition_id\" = 100"
    build = Build.find_by!(id: 2, partition_id: 100)

    result = QueryRecorder.log do
      build.delete
    end

    _(result).must_include(delete_statement)
  end

  it 'destroys using id and partition_id' do
    destroy_statement = "DELETE FROM \"builds\" WHERE \"builds\".\"id\" = 3 AND \"builds\".\"partition_id\" = 100"
    build = Build.find_by!(id: 3, partition_id: 100)

    result = QueryRecorder.log do
      build.destroy
    end

    _(result).must_include(destroy_statement)
  end

  it 'deletes_all using partition_id' do
    delete_statement = "DELETE FROM \"builds\" WHERE \"builds\".\"partition_id\" = 200"

    result = QueryRecorder.log do
      Build.where(partition_id: 200).delete_all
    end

    _(result).must_include(delete_statement)
  end

  it 'destroy_all using partition_id' do
    destroy_statement = "DELETE FROM \"builds\" WHERE \"builds\".\"id\" = 31 AND \"builds\".\"partition_id\" = 300"

    result = QueryRecorder.log do
      Build.where(partition_id: 300).destroy_all
    end

    _(result).must_include(destroy_statement)
  end

  it 'counts using partition_id' do
    count_statement = "SELECT COUNT(*) FROM \"builds\" WHERE \"builds\".\"partition_id\" = 400"

    result = QueryRecorder.log do
      Build.where(partition_id: 400).count
    end

    _(result).must_include(count_statement)
  end
end

# associations:
#  finding records
#  adding records
#  removing records
#  aggregate functions

describe 'has_many association queries' do
  before do
    @pipeline = Pipeline.find_by(id: 2, partition_id: 100)
  end

  it 'finds individual records using partition_id' do
    find_statement = "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"pipeline_id\" = 2 AND \"builds\".\"partition_id\" = 100 AND \"builds\".\"id\" = 4 LIMIT 1"

    result = QueryRecorder.log do
      @pipeline.builds.find(4)
    end

    _(result).must_include(find_statement)
  end

  it 'finds all records using partition_id' do
    find_statement = "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"pipeline_id\" = 2 AND \"builds\".\"partition_id\" = 100"

    result = QueryRecorder.log do
      @pipeline.builds.all.to_a
    end

    _(result).must_include(find_statement)
  end

  it 'builds records using partition_id' do
    build = @pipeline.builds.new(name: 'test job')

    _(build.pipeline_id).must_equal(@pipeline.id)
    _(build.partition_id).must_equal(@pipeline.partition_id)
  end

  it 'saves records using partition_id' do
    create_statement = "INSERT INTO \"builds\" (\"pipeline_id\", \"partition_id\", \"name\") VALUES (2, 100, 'test job')"

    result = QueryRecorder.log do
      build = @pipeline.builds.new(name: 'test job')
      build.save!
    end

    _(result).must_include(create_statement)
  end

  it 'creates records using partition_id' do
    create_statement = "INSERT INTO \"builds\" (\"pipeline_id\", \"partition_id\", \"name\") VALUES (2, 100, 'test job')"

    result = QueryRecorder.log do
      @pipeline.builds.create!(name: 'test job')
    end

    _(result).must_include(create_statement)
  end

  it 'deletes_all records using partition_id' do
    delete_statement = "DELETE FROM \"builds\" WHERE \"builds\".\"pipeline_id\" = 2 AND \"builds\".\"partition_id\" = 100"

    result = QueryRecorder.log do
      @pipeline.builds.delete_all
    end

    _(result).must_include(delete_statement)
  end

  it 'destroy_all records using partition_id' do
    destroy_statement = "DELETE FROM \"builds\" WHERE \"builds\".\"id\" = 4 AND \"builds\".\"partition_id\" = 100"

    result = QueryRecorder.log do
      @pipeline.builds.destroy_all
    end

    _(result).must_include(destroy_statement)
  end

  it 'counts records using partition_id' do
    destroy_statement = "SELECT COUNT(*) FROM \"builds\" WHERE \"builds\".\"pipeline_id\" = 2 AND \"builds\".\"partition_id\" = 100"

    result = QueryRecorder.log do
      @pipeline.builds.count
    end

    _(result).must_include(destroy_statement)
  end
end

describe 'belongs_to association queries' do
  before do
    @build = Build.find_by(id: 2, partition_id: 100)
  end

  it 'finds associated record using partition_id' do
    find_statement = "SELECT \"pipelines\".* FROM \"pipelines\" WHERE \"pipelines\".\"id\" = 1 AND \"pipelines\".\"partition_id\" = 100 LIMIT 1"

    result = QueryRecorder.log do
      @build.pipeline
    end

    _(result).must_include(find_statement)
  end

  it 'builds records using partition_id' do
    pipeline = @build.build_pipeline

    _(pipeline.partition_id).must_equal(@build.partition_id)
  end

  it 'saves records using partition_id' do
    create_statement = "INSERT INTO \"pipelines\" (\"partition_id\") VALUES (100)"

    result = QueryRecorder.log do
      @build.build_pipeline.save!
    end

    _(result).must_include(create_statement)
  end

  it 'creates records using partition_id' do
    create_statement = "INSERT INTO \"pipelines\" (\"partition_id\") VALUES (100)"

    result = QueryRecorder.log do
      @build.create_pipeline!
    end

    _(result).must_include(create_statement)
  end
end

describe 'has_one association queries' do
  before do
    @build = Build.find_by(id: 2, partition_id: 100)
  end

  it 'finds associated record using partition_id' do
    find_statement = "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"build_id\" = 2 AND \"builds_metadata\".\"partition_id\" = 100 LIMIT 1"

    result = QueryRecorder.log do
      @build.metadata
    end

    _(result).must_include(find_statement)
  end

  it 'builds records using partition_id' do
    metadata = @build.build_metadata

    _(metadata.build_id).must_equal(@build.id)
    _(metadata.partition_id).must_equal(@build.partition_id)
  end

  it 'saves records using partition_id' do
    create_statement = "INSERT INTO \"builds_metadata\" (\"build_id\", \"partition_id\") VALUES (2, 100)"

    result = QueryRecorder.log do
      @build.build_metadata.save!
    end

    _(result).must_include(create_statement)
  end

  it 'creates records using partition_id' do
    create_statement = "INSERT INTO \"builds_metadata\" (\"build_id\", \"partition_id\") VALUES (2, 100)"

    result = QueryRecorder.log do
      @build.create_metadata
    end

    _(result).must_include(create_statement)
  end

  it 'uses nested attributes on create' do
    skip
    insert_statements = [
      "INSERT INTO \"builds\" (\"pipeline_id\", \"partition_id\", \"name\") VALUES (2, 100, 'test')",
      "INSERT INTO \"builds_metadata\" (\"build_id\", \"partition_id\", \"test_flag\") VALUES (46, 100, 1)"
    ]

    pipeline = Pipeline.find_by(id: 2, partition_id: 100)

    result = QueryRecorder.log do
      pipeline.builds.create!(name: 'test', metadata_attributes: { test_flag: true })
    end

    insert_statements.each do |statement|
      _(result).must_include(statement)
    end
  end

  it 'uses nested attributes on update' do
    update_statements = [
      "UPDATE \"builds\" SET \"name\" = 'other test' WHERE \"builds\".\"id\" = 2 AND \"builds\".\"partition_id\" = 100",
      "INSERT INTO \"builds_metadata\" (\"build_id\", \"partition_id\", \"test_flag\") VALUES (2, 100, 1)"
    ]

    @build.name = 'other test'
    @build.metadata_attributes = { test_flag: true }

    result = QueryRecorder.log do
      @build.save!
    end

    update_statements.each do |statement|
      _(result).must_include(statement)
    end
  end
end

# joins
# through joins

describe 'joins queries' do
  it 'joins using partition_id' do
    join_statement =  "SELECT \"pipelines\".* FROM \"pipelines\" INNER JOIN \"builds\" ON \"builds\".\"pipeline_id\" = \"pipelines\".\"id\" AND \"builds\".\"partition_id\" = \"pipelines\".\"partition_id\" WHERE \"pipelines\".\"partition_id\" = 100"

    result = QueryRecorder.log do
      Pipeline.where(partition_id: 100).joins(:builds).to_a
    end

    _(result).must_include(join_statement)
  end

  it 'joins other models using partition_id' do
    join_statement = "SELECT \"pipelines\".* FROM \"pipelines\" INNER JOIN \"builds\" ON \"builds\".\"pipeline_id\" = \"pipelines\".\"id\" AND \"builds\".\"partition_id\" = \"pipelines\".\"partition_id\" INNER JOIN \"builds_metadata\" ON \"builds_metadata\".\"build_id\" = \"builds\".\"id\" AND \"builds_metadata\".\"partition_id\" = \"builds\".\"partition_id\" WHERE \"pipelines\".\"partition_id\" = 100"

    result = QueryRecorder.log do
      Pipeline.where(partition_id: 100).joins(builds: :metadata).to_a
    end

    _(result).must_include(join_statement)
  end
end

# single relation preload
# preloads hash
# join preloads

describe 'preload queries with single partition' do
  it 'preloads metadata for builds' do
    preload_statements = [
      "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 100",
      "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"partition_id\" = 100 AND \"builds_metadata\".\"build_id\" IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)"
    ]

    result = QueryRecorder.log do
      Build.where(partition_id: 100).preload(:metadata).to_a
    end

    preload_statements.each do |statement|
      _(result).must_include(statement)
    end
  end

  it 'preloads builds for pipelines' do
    preload_statements = [
      "SELECT \"pipelines\".* FROM \"pipelines\" WHERE \"pipelines\".\"partition_id\" = 100",
      "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 100 AND \"builds\".\"pipeline_id\" IN (1, 2, 3, 4, 5)"
    ]

    result = QueryRecorder.log do
      Pipeline.where(partition_id: 100).preload(:builds).to_a
    end

    preload_statements.each do |statement|
      _(result).must_include(statement)
    end
  end

  it 'preloads builds and metadata for pipelines' do
    preload_statements = [
      "SELECT \"pipelines\".* FROM \"pipelines\" WHERE \"pipelines\".\"partition_id\" = 100",
      "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 100 AND \"builds\".\"pipeline_id\" IN (1, 2, 3, 4, 5)",
      "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"partition_id\" = 100 AND \"builds_metadata\".\"build_id\" IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)"
    ]

    result = QueryRecorder.log do
      Pipeline.where(partition_id: 100).preload(builds: :metadata).to_a
    end

    preload_statements.each do |statement|
      _(result).must_include(statement)
    end
  end
end

describe 'preload queries with multiple partitions' do
  it 'preloads metadata for builds' do
    preload_statements = [
      "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" IN (100, 200)",
      "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"partition_id\" = 100 AND \"builds_metadata\".\"build_id\" IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)",
      "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"partition_id\" = 200 AND \"builds_metadata\".\"build_id\" IN (16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30)"
    ]

    result = QueryRecorder.log do
      Build.where(partition_id: [100, 200]).preload(:metadata).to_a
    end

    preload_statements.each do |statement|
      _(result).must_include(statement)
    end
  end

  it 'preloads builds for pipelines' do
    preload_statements = [
      "SELECT \"pipelines\".* FROM \"pipelines\" WHERE \"pipelines\".\"partition_id\" IN (100, 200)",
      "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 100 AND \"builds\".\"pipeline_id\" IN (1, 2, 3, 4, 5)",
      "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 200 AND \"builds\".\"pipeline_id\" IN (6, 7, 8, 9, 10)"
    ]

    result = QueryRecorder.log do
      Pipeline.where(partition_id: [100, 200]).preload(:builds).to_a
    end

    preload_statements.each do |statement|
      _(result).must_include(statement)
    end
  end

  it 'preloads builds and metadata for pipelines' do
    preload_statements = [
     "SELECT \"pipelines\".* FROM \"pipelines\" WHERE \"pipelines\".\"partition_id\" IN (100, 200)",
     "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 100 AND \"builds\".\"pipeline_id\" IN (1, 2, 3, 4, 5)",
     "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 200 AND \"builds\".\"pipeline_id\" IN (6, 7, 8, 9, 10)",
     "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"partition_id\" = 100 AND \"builds_metadata\".\"build_id\" IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)",
     "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"partition_id\" = 200 AND \"builds_metadata\".\"build_id\" IN (16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30)"
    ]

    result = QueryRecorder.log do
      Pipeline.where(partition_id: [100, 200]).preload(builds: :metadata).to_a
    end

    preload_statements.each do |statement|
      _(result).must_include(statement)
    end
  end
end

# https://api.rubyonrails.org/classes/ActiveRecord/QueryMethods.html#method-i-includes

describe 'includes queries' do
  before do
    @project = Project.first
  end

  # https://gitlab.com/gitlab-org/gitlab/-/blob/46e0100b86d5539b422e56f92d9cf7256fd60435/app/controllers/projects/pipelines_controller.rb#L284-287
  it 'preloads data for pipeline with multiple queries' do
    preload_statements = [
      "SELECT \"pipelines\".* FROM \"pipelines\" WHERE \"pipelines\".\"project_id\" = 1 AND \"pipelines\".\"id\" IN (1, 2) AND \"pipelines\".\"partition_id\" = 100",
      "SELECT \"builds\".* FROM \"builds\" WHERE \"builds\".\"partition_id\" = 100 AND \"builds\".\"pipeline_id\" IN (1, 2)",
      "SELECT \"builds_metadata\".* FROM \"builds_metadata\" WHERE \"builds_metadata\".\"partition_id\" = 100 AND \"builds_metadata\".\"build_id\" IN (1, 2, 3, 4, 5, 6)"
    ]

    result = QueryRecorder.log do
      @project.pipelines.includes(builds: :metadata).where(id: [1, 2], partition_id: 100).to_a
    end

    preload_statements.each do |statement|
      _(result).must_include(statement)
    end
  end

  # I'm not sure if we use `includes` with `references`, but added it for completion.

  it 'preloads data for pipeline with join query' do
    preload_statement = "SELECT \"pipelines\".\"id\" AS t0_r0, \"pipelines\".\"project_id\" AS t0_r1, \"pipelines\".\"partition_id\" AS t0_r2, \"builds\".\"id\" AS t1_r0, \"builds\".\"pipeline_id\" AS t1_r1, \"builds\".\"partition_id\" AS t1_r2, \"builds\".\"name\" AS t1_r3, \"builds_metadata\".\"id\" AS t2_r0, \"builds_metadata\".\"build_id\" AS t2_r1, \"builds_metadata\".\"partition_id\" AS t2_r2, \"builds_metadata\".\"test_flag\" AS t2_r3 FROM \"pipelines\" LEFT OUTER JOIN \"builds\" ON \"builds\".\"pipeline_id\" = \"pipelines\".\"id\" AND \"builds\".\"partition_id\" = \"pipelines\".\"partition_id\" LEFT OUTER JOIN \"builds_metadata\" ON \"builds_metadata\".\"build_id\" = \"builds\".\"id\" AND \"builds_metadata\".\"partition_id\" = \"builds\".\"partition_id\" WHERE \"pipelines\".\"project_id\" = 1 AND \"pipelines\".\"id\" IN (1, 2) AND \"pipelines\".\"partition_id\" = 100"

    result = QueryRecorder.log do
      @project.pipelines.includes(builds: :metadata).references(:builds, :metadata).where(id: [1, 2], partition_id: 100).to_a
    end

    _(result).must_include(preload_statement)
  end
end
